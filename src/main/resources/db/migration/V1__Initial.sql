-- Table: "user"

-- DROP TABLE "user";

CREATE TABLE "user"
(
  id serial NOT NULL,
  username character varying(20) NOT NULL,
  password character varying(100) NOT NULL,
  CONSTRAINT user_pkey PRIMARY KEY (id),
  CONSTRAINT user_username_unique UNIQUE (username)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "user"
  OWNER TO postgres;

-- Table: user_role

-- DROP TABLE user_role;

CREATE TABLE user_role
(
  user_id integer NOT NULL,
  role_id integer NOT NULL,
  CONSTRAINT pk_user_role PRIMARY KEY (user_id, role_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_role
  OWNER TO postgres;

-- Table: ticket

-- DROP TABLE ticket;

CREATE TABLE ticket
(
  id serial NOT NULL,
  title character varying(15) NOT NULL,
  state character varying(5) NOT NULL,
  description character varying(50) NOT NULL,
  user_id integer NOT NULL,
  CONSTRAINT ticket_pkey PRIMARY KEY (id),
  CONSTRAINT fk_ticket_user_id_id FOREIGN KEY (user_id)
      REFERENCES "user" (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
)
WITH (
  OIDS=FALSE
);
ALTER TABLE ticket
  OWNER TO postgres;

-- Table: role

-- DROP TABLE role;

CREATE TABLE role
(
  id serial NOT NULL,
  role_name character varying(12) NOT NULL,
  CONSTRAINT role_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE role
  OWNER TO postgres;

-- Table: answer

-- DROP TABLE answer;

CREATE TABLE answer
(
  id serial NOT NULL,
  description character varying(100) NOT NULL,
  "ticketId" integer NOT NULL,
  CONSTRAINT answer_pkey PRIMARY KEY (id),
  CONSTRAINT fk_answer_ticketid_id FOREIGN KEY ("ticketId")
      REFERENCES ticket (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
)
WITH (
  OIDS=FALSE
);
ALTER TABLE answer
  OWNER TO postgres;
