package com.example.demo

import org.jetbrains.exposed.sql.Database
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

/**
 * Created by Saeed on 7/27/2019.
 */

@SpringBootApplication
open class TicketSupportSystemApplication

fun main(args: Array<String>) {
    SpringApplication.run(TicketSupportSystemApplication::class.java, *args)

    // connect to DB
    Database.connect(
            "jdbc:postgresql://localhost:5432/ticket_system",
            driver = "org.postgresql.Driver",
            user = "postgres",
            password = "root")

    // insert some data
//    transaction {
//        val role1 = Role.new {
//            roleName = RoleName.ROLE_USER
//        }
//        val role2 = Role.new {
//            roleName = RoleName.ROLE_SUPPORT
//        }
//        val role3 = Role.new {
//            roleName = RoleName.ROLE_ADMIN
//        }

//        val role1 = Role.get(1)
//        val role2 = Role.get(2)
//        val role3 = Role.get(3)
//
//        val user1 = User.new {
//            username = "saeed"
//            password = "12345"
//        }
//
//        val user2 = User.new {
//            username = "ali"
//            password = "12345"
//        }
//
//        user1.roles = SizedCollection(listOf(role1, role2, role3))
//        user2.roles = SizedCollection(listOf(role1))
//    }
}