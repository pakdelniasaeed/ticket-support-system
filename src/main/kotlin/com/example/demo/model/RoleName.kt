package com.example.demo.model

/**
 * Created by Saeed on 7/28/2019.
 */
enum class RoleName {
    ROLE_USER,
    ROLE_SUPPORT,
    ROLE_ADMIN
}