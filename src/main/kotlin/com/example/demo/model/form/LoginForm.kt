package com.example.demo.model.form

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

/**
 * Created by Saeed on 7/28/2019.
 */
class LoginForm
constructor(
        @NotBlank
        @NotNull
        @NotEmpty
        @Size(min = 3, max = 20)
        val username: String,
        @NotBlank
        @NotNull
        @NotEmpty
        @Size(min = 5, max = 20)
        val password: String) {
}