package com.example.demo.model.form

/**
 * Created by Saeed on 8/3/2019.
 */
class TicketForm (
        val title: String,
        val description: String
){
}