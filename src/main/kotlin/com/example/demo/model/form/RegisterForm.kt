package com.example.demo.model.form

/**
 * Created by Saeed on 7/28/2019.
 */
class RegisterForm constructor(
        val username: String,
        val password: String,
        val roles: List<String>
) {
}