package com.example.demo.model.form

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by Saeed on 8/4/2019.
 */
class AnswerForm(@JsonProperty("description")val description: String) {}