package com.example.demo.model

/**
 * Created by Saeed on 8/4/2019.
 */
class Answer(val id: Int,
             val description: String,
             val ticket: Ticket)