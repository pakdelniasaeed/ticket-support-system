package com.example.demo.model


/**
 * Created by Saeed on 7/28/2019.
 */
data class Role(
        val id: Int,
        val roleName: RoleName
){
}