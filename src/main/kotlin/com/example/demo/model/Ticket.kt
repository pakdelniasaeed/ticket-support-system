package com.example.demo.model

/**
 * Created by Saeed on 8/3/2019.
 */
class Ticket(
        var id: Int,
        var title: String,
        var description: String,
        var state: State,
        var user: User,
        var answer: String
)