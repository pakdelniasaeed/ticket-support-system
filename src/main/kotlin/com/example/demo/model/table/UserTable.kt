package com.example.demo.model.table

import org.jetbrains.exposed.sql.Table

/**
 * Created by Saeed on 7/28/2019.
 */

object UserTable : Table(name = "user") {

    val id = integer("id").autoIncrement().primaryKey()
    val username = com.example.demo.model.table.UserTable.varchar("username", 20).uniqueIndex()
    val password = com.example.demo.model.table.UserTable.varchar("password", 100)
}
