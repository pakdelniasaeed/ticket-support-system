package com.example.demo.model.table

import com.example.demo.model.State
import org.jetbrains.exposed.sql.Table

/**
 * Created by Saeed on 8/3/2019.
 */
object TicketTable : Table("ticket"){
    val id = integer("id").autoIncrement().primaryKey()
    val title = varchar("title", 15)
    val state = enumerationByName("state", 5, State::class)
    val description = varchar("description", 50)
    val userId = integer("user_id").references(UserTable.id)
}