package com.example.demo.model.table

import org.jetbrains.exposed.sql.Table

/**
 * Created by Saeed on 7/28/2019.
 */
object User_Roles: Table("user_role") {
    val userId = integer("user_id").primaryKey(0)
    val roleId = integer("role_id").primaryKey(1)
}