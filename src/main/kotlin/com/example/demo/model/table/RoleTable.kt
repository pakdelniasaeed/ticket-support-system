package com.example.demo.model.table

import com.example.demo.model.RoleName
import org.jetbrains.exposed.sql.Table

/**
 * Created by Saeed on 7/28/2019.
 */

object RoleTable: Table(name = "role") {
    val id = integer("id").autoIncrement().primaryKey()
    val roleName = enumerationByName("role_name", 12, RoleName::class)
}