package com.example.demo.model.table

import org.jetbrains.exposed.sql.Table

/**
 * Created by Saeed on 8/4/2019.
 */

object AnswerTable : Table("answer") {
    val id = integer("id").autoIncrement().primaryKey()
    val description = varchar("description", 100)
    val ticketId = integer("ticket_id").references(TicketTable.id)
}