package com.example.demo.model

import com.fasterxml.jackson.annotation.JsonIgnore


/**
 * Created by Saeed on 7/28/2019.
 */
class User {
    var id: Int = 0
    var username: String = ""
    @JsonIgnore
    var password: String = ""
//    @JsonIgnore
    var roles: List<Role>? = null

    constructor()

    constructor(id: Int, username: String, password: String, roles: List<Role>) {
        this.id = id
        this.username = username
        this.password = password
        this.roles = roles
    }

    constructor(id: Int) {
        this.id = id
    }

}