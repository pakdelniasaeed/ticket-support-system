package com.example.demo.model

/**
 * Created by Saeed on 8/3/2019.
 */
enum class State {
    WAIT,
    OPEN,
    CLOSE
}