package com.example.demo.controller

import com.example.demo.model.User
import com.example.demo.repository.UserRepository
import com.example.demo.repository.UserRolesRepository
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Created by Saeed on 8/7/2019.
 */

@RestController
@RequestMapping("/api/user")
open class UserRestController(
        private val userRolesRepository: UserRolesRepository,
        private val userRepository: UserRepository
) {

    @GetMapping("/list")
    @PreAuthorize("hasRole('ADMIN')")
    open fun listAllUsers(): ResponseEntity<List<User>> {
        return ResponseEntity(userRepository.findAll(), HttpStatus.OK)
    }

    @GetMapping("/tosupport/{userId}")
    @PreAuthorize("hasRole('ADMIN')")
    open fun userToSupport(@PathVariable("userId") userId: Int): ResponseEntity<String> {
        userRolesRepository.addSupportRole(userId)
        return ResponseEntity("user role changed to support", HttpStatus.OK)
    }

    @GetMapping("/removesupport/{userId}")
    @PreAuthorize("hasRole('ADMIN')")
    open fun removeSupport(@PathVariable("userId") userId: Int): ResponseEntity<String> {
        userRolesRepository.removeSupportRole(userId)
        return ResponseEntity("user role_support removed", HttpStatus.OK)
    }
}