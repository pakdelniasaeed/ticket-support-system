package com.example.demo.controller

import com.example.demo.model.Answer
import com.example.demo.model.State
import com.example.demo.model.Ticket
import com.example.demo.model.User
import com.example.demo.model.form.AnswerForm
import com.example.demo.model.form.TicketForm
import com.example.demo.repository.AnswerRepository
import com.example.demo.repository.TicketRepository
import com.example.demo.security.service.UserPrinciple
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.*

/**
 * Created by Saeed on 8/3/2019.
 */

@RestController
@RequestMapping("/api/ticket")
open class TicketRestController(
        private val ticketRepository: TicketRepository,
        private val answerRepository: AnswerRepository,
        private val simpleMessagingTemplate: SimpMessagingTemplate
) {

    @PostMapping
    open fun createTicket(@RequestBody ticket: TicketForm,
                          auth: Authentication): ResponseEntity<String> {

        val priciple = auth.principal as UserPrinciple
        val user: User = User()
        user.id = priciple.getId()
        user.username = priciple.username

        val ticket = Ticket(0, ticket.title, ticket.description, State.WAIT, user, "")
        val id = ticketRepository.save(ticket)

        // web socket: send user action to admin/support
        simpleMessagingTemplate
                .convertAndSend("/topic/action",
                        "USER: " + user.id + "-" + user.username + " create new ticket: " + ticket.title)

        return ResponseEntity("ticket saved by id: " + id, HttpStatus.OK)
    }

    @GetMapping
    open fun getAllTicketsByUserId(auth: Authentication): ResponseEntity<List<Ticket>> {
        val userId = (auth.principal as UserPrinciple).getId()
        return ResponseEntity(ticketRepository.findByUserId(userId), HttpStatus.OK)
    }

    @GetMapping("/list")
    @PreAuthorize("hasRole('SUPPORT') or hasRole('ADMIN')")
    open fun getAllTickets(): ResponseEntity<List<Ticket>> {
        return ResponseEntity(ticketRepository.findAll(), HttpStatus.OK)
    }

    @GetMapping("/close/{ticketId}")
    @PreAuthorize("hasRole('SUPPORT') or hasRole('ADMIN')")
    open fun closeTicket(@PathVariable("ticketId") ticketId: Int, auth: Authentication): ResponseEntity<String> {

        ticketRepository.closeTicket(ticketId)

        val user = auth.principal as UserPrinciple
        // web socket: send user action to admin/support
        simpleMessagingTemplate
                .convertAndSend("/topic/action",
                        "SUPPORT: " + user.getId() + "-" + user.username + " closed ticket: " + ticketId)
        return ResponseEntity("ticket closed", HttpStatus.OK)
    }

    @PostMapping("/answer/{ticketId}")
    @PreAuthorize("hasRole('SUPPORT') or hasRole('ADMIN')")
    open fun answerTicket(@PathVariable("ticketId") ticketId: Int,
                          auth: Authentication,
                          @RequestBody answerForm: AnswerForm): ResponseEntity<String> {

        val user = auth.principal as UserPrinciple

        ticketRepository.openTicket(ticketId)
        answerRepository.save(Answer(0, answerForm.description, Ticket(ticketId, "", "", State.WAIT, User(), ""))) // todo this is bad :(

        // web socket: send user action to admin/support
        simpleMessagingTemplate
                .convertAndSend("/topic/action",
                        "SUPPORT: " + user.getId() + "-" + user.username + " answered ticket: " + ticketId + ": " + answerForm.description)

        return ResponseEntity("ticket answered!", HttpStatus.OK)
    }
}