package com.example.demo.controller

import com.example.demo.model.Role
import com.example.demo.model.User
import com.example.demo.model.form.LoginForm
import com.example.demo.model.form.RegisterForm
import com.example.demo.repository.RoleRepository
import com.example.demo.repository.UserRepository
import com.example.demo.security.jwt.JwtProvider
import org.jetbrains.exposed.sql.transactions.transaction
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

/**
 * Created by Saeed on 7/28/2019.
 */

@RestController
@RequestMapping("/api/auth")
class AuthRestController(
        private val jwtProvider: JwtProvider,
        private val authenticationManager: AuthenticationManager,
        private val passwordEncoder: PasswordEncoder,
        private val userRepository: UserRepository,
        private val roleRepository: RoleRepository
) {

//    @CrossOrigin("*")
    @PostMapping("/login")
    fun authenticateUser(@Valid @RequestBody loginForm: LoginForm)
            : ResponseEntity<String> {

        val auth: Authentication = authenticationManager.authenticate(
                UsernamePasswordAuthenticationToken(
                        loginForm.username,
                        loginForm.password
                )
        )

        SecurityContextHolder.getContext().authentication = auth // no need to be here

        val jwt: String = jwtProvider.GenerateJwtToken(auth)

        return ResponseEntity(jwt, HttpStatus.OK)
    }

    @PostMapping("/register")
    fun registerUser(@RequestBody registerForm: RegisterForm): ResponseEntity<String> {

        if (userRepository.existByUsername(registerForm.username)){
            return ResponseEntity("username is taken!", HttpStatus.BAD_REQUEST)
        }

        val user: User = User()
        user.username = registerForm.username
        user.password = passwordEncoder.encode(registerForm.password)

        // get roles
        var roles : MutableList<Role> = ArrayList<Role>()
        transaction {
            registerForm.roles.forEach {
                roles.add(roleRepository.findByRoleName(it))
            }
            user.roles = roles
        }

        val id: Int = userRepository.save(user)

        return ResponseEntity("user saved by id = "+id, HttpStatus.CREATED)
    }
}