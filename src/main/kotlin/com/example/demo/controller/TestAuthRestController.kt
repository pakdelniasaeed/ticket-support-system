package com.example.demo.controller

import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Created by Saeed on 8/1/2019.
 */

@RestController
open class TestAuthRestController {

    @GetMapping("/api/test/user")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    open fun userAccess(): String {


        return "User Access!"
    }

    @GetMapping("/api/test/support")
    @PreAuthorize("hasRole('SUPPORT') or hasRole('ADMIN')")
    open fun supportAccess(): String {
        return "Support Access!"
    }

    @GetMapping("/api/test/admin")
    @PreAuthorize("hasRole('ADMIN')")
    open fun adminAccess(): String {
        return "Admin Access!"
    }
}