package com.example.demo.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Created by Saeed on 7/27/2019.
 */

@RestController
class HelloController {

    @GetMapping("/api/hello")
    fun hello(): String {
        return "hello :)"
    }
}