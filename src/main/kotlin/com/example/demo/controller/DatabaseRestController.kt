package com.example.demo.controller

import com.example.demo.model.RoleName
import com.example.demo.model.table.*
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Created by Saeed on 7/30/2019.
 */

@RestController
class DatabaseRestController {

    // create tables
    @GetMapping("/api/auth/create")
    fun create(): String {
        transaction {
            SchemaUtils.create(UserTable, RoleTable, User_Roles, TicketTable, AnswerTable)
        }
        return "Created!"
    }

    // init role
    @GetMapping("/api/auth/init/role")
    fun initRole(): String {
        transaction {
            RoleTable.insert { it[RoleTable.roleName] = RoleName.ROLE_USER }
            RoleTable.insert { it[RoleTable.roleName] = RoleName.ROLE_SUPPORT }
            RoleTable.insert { it[RoleTable.roleName] = RoleName.ROLE_ADMIN }
        }
        return "Role Initialized!"
    }
}