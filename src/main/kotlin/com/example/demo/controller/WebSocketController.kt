package com.example.demo.controller

import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.stereotype.Controller
import org.springframework.web.util.HtmlUtils

/**
 * Created by Saeed on 8/3/2019.
 */

@Controller
class WebSocketController {

    // todo user support id, username must be in message
    // todo action object
    @MessageMapping("/action")
    @SendTo("/topic/action")
    fun action(action: String): String = "SUPPORT: supportId: action this: " + HtmlUtils.htmlEscape(action) + "!"
}