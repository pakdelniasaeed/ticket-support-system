package com.example.demo.repository

import com.example.demo.model.Answer
import com.example.demo.model.State
import com.example.demo.model.Ticket
import com.example.demo.model.User
import com.example.demo.model.table.AnswerTable
import com.example.demo.model.table.TicketTable
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import org.springframework.stereotype.Repository

/**
 * Created by Saeed on 8/4/2019.
 */

@Repository
open class AnswerRepository {
    fun save(answer: Answer): Int {
        var id: Int = 0
        transaction {
            if (!AnswerTable.select { AnswerTable.ticketId eq answer.ticket.id }.empty()){
                id = AnswerTable.update({ AnswerTable.ticketId eq answer.ticket.id }) {
                    it[AnswerTable.description] = answer.description
                    it[AnswerTable.ticketId] = answer.ticket.id
                }
            }else {
                id = AnswerTable.insert {
                    it[AnswerTable.description] = answer.description
                    it[AnswerTable.ticketId] = answer.ticket.id
                } get AnswerTable.id
            }
        }
        return id
    }

    fun findByTicketId(ticketId: Int): Answer {
        var answer: Answer = Answer(0,"", Ticket(0,"","",State.WAIT,User(),"")) // todo its bad :(
        transaction {
            try {
                answer = AnswerTable.select {AnswerTable.ticketId eq ticketId}.single().toAnswer()
            }catch (e: Exception) { return@transaction }
        }
        return answer
    }

    private fun ResultRow.toAnswer(): Answer = Answer(
            id = this[AnswerTable.id],
            description = this[AnswerTable.description],
            ticket = Ticket(this[AnswerTable.ticketId],"","",State.WAIT, User(),"") // todo its bad :(
    )
}