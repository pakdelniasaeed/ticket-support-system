package com.example.demo.repository

import com.example.demo.model.table.User_Roles
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction
import org.springframework.stereotype.Repository

/**
 * Created by Saeed on 8/7/2019.
 */

@Repository
open class UserRolesRepository {

    fun addSupportRole(userId: Int) {
        transaction {
            User_Roles.insert {
                it[User_Roles.userId] = userId
                it[User_Roles.roleId] = 2
            }
        }
    }

    fun removeSupportRole(userId: Int) {
        transaction {
            User_Roles.deleteWhere {
                User_Roles.userId eq userId and(User_Roles.roleId eq 2)
            }
        }
    }
}