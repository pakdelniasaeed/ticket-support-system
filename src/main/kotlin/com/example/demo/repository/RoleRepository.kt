package com.example.demo.repository

import com.example.demo.model.Role
import com.example.demo.model.RoleName
import com.example.demo.model.table.RoleTable
import com.example.demo.repository.crud.RoleCrudRepository
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.springframework.stereotype.Repository

/**
 * Created by Saeed on 8/1/2019.
 */

@Repository
open class RoleRepository: RoleCrudRepository {
    override fun createTable() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<Role> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Int): Role {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findByRoleName(roleName: String): Role {
        var role: Role? = null
        transaction {
            role = RoleTable.select { RoleTable.roleName eq RoleName.valueOf(roleName) }.single().toRole()
        }
        return role!!
    }

    fun ResultRow.toRole() = Role(
            id = this[RoleTable.id],
            roleName = this[RoleTable.roleName]
    )
}