package com.example.demo.repository.crud

import com.example.demo.model.User

/**
 * Created by Saeed on 7/30/2019.
 */
interface UserCrudRepository : CrudRepository<User>{
    fun findByUsername(username: String) : User
    fun save(user: User): Int
    fun existByUsername(username: String): Boolean
}