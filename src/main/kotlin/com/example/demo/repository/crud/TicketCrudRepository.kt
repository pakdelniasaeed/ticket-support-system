package com.example.demo.repository.crud

import com.example.demo.model.Ticket

/**
 * Created by Saeed on 8/3/2019.
 */
interface TicketCrudRepository : CrudRepository<Ticket>{
    fun save(ticket: Ticket): Int
    fun findByUserId(userId: Int): List<Ticket>
}