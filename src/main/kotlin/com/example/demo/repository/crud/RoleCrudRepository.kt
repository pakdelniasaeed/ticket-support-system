package com.example.demo.repository.crud

import com.example.demo.model.Role


/**
 * Created by Saeed on 8/1/2019.
 */
interface RoleCrudRepository: CrudRepository<Role> {
    fun findByRoleName(roleName: String): Role
}