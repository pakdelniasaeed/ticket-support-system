package com.example.demo.repository.crud

/**
 * Created by Saeed on 7/30/2019.
 */
interface CrudRepository<T> {

    fun createTable()
    fun findAll(): List<T>
    fun findById(id: Int): T
}