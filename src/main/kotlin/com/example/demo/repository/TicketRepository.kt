package com.example.demo.repository

import com.example.demo.model.State
import com.example.demo.model.Ticket
import com.example.demo.model.User
import com.example.demo.model.table.TicketTable
import com.example.demo.repository.crud.TicketCrudRepository
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.springframework.stereotype.Repository

/**
 * Created by Saeed on 8/3/2019.
 */

@Repository
open class  TicketRepository(
        private val userRepository: UserRepository,
        private val answerRepository: AnswerRepository
) : TicketCrudRepository{
    override fun save(ticket: Ticket): Int {
        var id = 0
        transaction {
            id = TicketTable.insert {
                it[TicketTable.title] = ticket.title
                it[TicketTable.description] = ticket.description
                it[TicketTable.userId] = ticket.user.id
                it[TicketTable.state] = ticket.state
            } get TicketTable.id
        }
        return id
    }

    override fun createTable() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<Ticket> {
        val ticketList: MutableList<Ticket> = ArrayList()
        transaction {
            for (t in TicketTable.selectAll()){
                val ticket = Ticket(
                        t[TicketTable.id],
                        t[TicketTable.title],
                        t[TicketTable.description],
                        t[TicketTable.state],
                        userRepository.findById(t[TicketTable.userId]),
                        answerRepository.findByTicketId(t[TicketTable.id]).description
                )
                ticketList.add(ticket)
            }
        }
        return ticketList
    }

    override fun findById(id: Int): Ticket {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findByUserId(userId: Int): List<Ticket> {
        val ticketList: MutableList<Ticket> = ArrayList()
        transaction {
            val list = TicketTable.select {TicketTable.userId eq userId}.forEach {
                ticketList.add(it.toTicket())
            }
        }
        return ticketList
    }

    fun closeTicket(id: Int) {
        transaction {
            TicketTable.update({TicketTable.id eq id }) {
                it[TicketTable.state] = State.CLOSE
            }
        }
    }

    fun openTicket(id: Int) {
        transaction {
            TicketTable.update({TicketTable.id eq id }) {
                it[TicketTable.state] = State.OPEN
            }
        }
    }

    private fun ResultRow.toTicket(): Ticket = Ticket(
            id = this[TicketTable.id],
            title = this[TicketTable.title],
            description = this[TicketTable.description],
            state = this[TicketTable.state],
            user = User(this[TicketTable.userId]),
            answer = answerRepository.findByTicketId(this[TicketTable.id]).description
    )

}