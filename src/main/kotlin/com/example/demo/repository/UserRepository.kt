package com.example.demo.repository

import com.example.demo.model.Role
import com.example.demo.model.RoleName
import com.example.demo.model.User
import com.example.demo.model.table.RoleTable
import com.example.demo.model.table.UserTable
import com.example.demo.model.table.User_Roles
import com.example.demo.repository.crud.UserCrudRepository
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.springframework.stereotype.Repository

/**
 * Created by Saeed on 7/29/2019.
 */

@Repository
open class UserRepository(
        private val roleRepository: RoleRepository
) : UserCrudRepository {
    override fun existByUsername(username: String): Boolean {
        try {
            if (findByUsername(username).username == username)
                return true
        } catch (ne: NullPointerException) {
            return false
        }

        return false
    }

    override fun createTable() {
        transaction {
            SchemaUtils.create(UserTable)
        }
    }

    override fun findAll(): List<User> {
        val userList: MutableList<User> = ArrayList()
        transaction {
            for (it in UserTable.selectAll()) {
                val user = it.toUser()
                val roles: MutableList<Role> = ArrayList()

                // find roles for this user
                UserTable.innerJoin(User_Roles, {UserTable.id}, {User_Roles.userId})
                        .selectAll().forEach {
                            if (it[UserTable.id] == user.id) {
                                when (it[User_Roles.roleId]){
                                    1 -> roles.add(Role(1, RoleName.ROLE_USER))
                                    2 -> roles.add(Role(2, RoleName.ROLE_SUPPORT))
                                    3 -> roles.add(Role(3, RoleName.ROLE_ADMIN))
                                }
                            }
                        }
                user.roles = roles
                userList.add(user)
            }
        }
        return userList
    }

    override fun findById(id: Int): User {
        var user: User? = null
        var roles: MutableList<Role> = ArrayList()
        transaction {
            user = UserTable.select { UserTable.id eq id }.single().toUser()

            // find roles for this user
            UserTable.innerJoin(User_Roles, {UserTable.id}, {User_Roles.userId})
                    .selectAll().forEach {
                if (it[UserTable.id] == id) {
                    when (it[User_Roles.roleId]){
                        1 -> roles.add(Role(1, RoleName.ROLE_USER))
                        2 -> roles.add(Role(2, RoleName.ROLE_SUPPORT))
                        3 -> roles.add(Role(3, RoleName.ROLE_ADMIN))
                    }
                }
            }
            user?.roles = roles
        }
        return user!!
    }

    override fun findByUsername(username: String): User {
        var user: User? = null
        var roles: MutableList<Role> = ArrayList()
        transaction {
            user = UserTable.select { UserTable.username eq username }.single().toUser()

            // find roles for this user
            UserTable.innerJoin(User_Roles, {UserTable.id}, {User_Roles.userId})
                    .selectAll().forEach {
                if (it[UserTable.username] == username) {
                    when (it[User_Roles.roleId]){
                        1 -> roles.add(Role(1, RoleName.ROLE_USER))
                        2 -> roles.add(Role(2, RoleName.ROLE_SUPPORT))
                        3 -> roles.add(Role(3, RoleName.ROLE_ADMIN))
                    }
                }
            }
            user?.roles = roles
        }
        return user!!
    }

    override fun save(user: User): Int {
        var id: Int = 0
        transaction {
            id = UserTable.insert {
                it[UserTable.username] = user.username
                it[UserTable.password] = user.password
            } get UserTable.id
            user.roles?.forEach { t ->
                User_Roles.insert {
                    it[User_Roles.userId] = id
                    it[User_Roles.roleId] = t.id
                }
            }
        }
        return id
    }

    fun ResultRow.toRole() = Role(
            id = this[RoleTable.id],
            roleName = this[RoleTable.roleName]
    )

    fun ResultRow.toUser() = User(
            id = this[UserTable.id],
            username = this[UserTable.username],
            password = this[UserTable.password],
            roles = emptyList()
    )

}
