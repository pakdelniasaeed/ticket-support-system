package com.example.demo.security

import com.example.demo.security.jwt.JwtAuthTokenFilter
import com.example.demo.security.jwt.JwtProvider
import com.example.demo.security.service.UserDetailServiceImp
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource

/**
 * Created by Saeed on 7/28/2019.
 */

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true) // its for @preAuthorize annotation work
open class WebSecurityConfig(
        private val userDetailServiceImp: UserDetailServiceImp,
        private val jwtProvider: JwtProvider
) : WebSecurityConfigurerAdapter() {


    override fun configure(http: HttpSecurity) {

        // look at corsConfigurationSource Bean
        http.cors()
        // no need csrf protection
        http.csrf().disable()

        // authentication requests required
        http.authorizeRequests()
                .antMatchers(
                        "/api/auth/**",
                        "/v2/api-docs/**",
                        "/swagger-ui.html/**",
                        "/webjars/**",
                        "/swagger-resources/**",
                        "/gs-guide-websocket/**"
                        )
                .permitAll()
                .anyRequest().authenticated()

        // doing stateless
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)

        // unauthorized error handler

        // add filter
        http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter::class.java)
    }

    override fun configure(auth: AuthenticationManagerBuilder) {
        auth
                .userDetailsService(userDetailServiceImp)
                .passwordEncoder(passwordEncoder())
    }

    @Bean
    open fun authenticationJwtTokenFilter(): JwtAuthTokenFilter {
        return JwtAuthTokenFilter(jwtProvider, userDetailServiceImp)
    }

    @Bean
    open fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Bean()
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }

    @Bean
    open fun corsConfigurationSource() : CorsConfigurationSource {
        val configuration = CorsConfiguration()
        configuration.allowedOrigins = listOf("*")
        configuration.allowedMethods = listOf("*")
        configuration.allowedHeaders = listOf("*")
        configuration.allowCredentials = true

        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", configuration)
        return source
    }
}