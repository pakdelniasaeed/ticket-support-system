package com.example.demo.security.jwt

import io.jsonwebtoken.*
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component
import java.util.*

/**
 * Created by Saeed on 7/28/2019.
 */

@Component
class JwtProvider {

    @Value("\${saeed.app.jwtSecretKey}")
    private val jwtSecretKey: String? = null

    @Value("\${saeed.app.jwtExpiration}")
    private val jwtExpiration: Int = 0

    fun GenerateJwtToken(authentication: Authentication): String {
        val userdetail: UserDetails = authentication.principal as UserDetails

        return Jwts.builder()
                .setSubject(userdetail.username)
                .setIssuedAt(Date())
                .setExpiration(Date(Date().time + (jwtExpiration * 1000)))
                .signWith(SignatureAlgorithm.HS512, jwtSecretKey)
                .compact()
    }

    fun validateJwtToken(jwt: String): Boolean {
        try {
            Jwts.parser().setSigningKey(jwtSecretKey).parseClaimsJws(jwt)
            return true
        } catch (se: SignatureException) {
            println("Invalid JWT signature -> Message: {} ")
        } catch (me: MalformedJwtException) {
            println("Invalid JWT token -> Message: {}")
        } catch (ee: ExpiredJwtException) {
            println("Expired JWT token -> Message: {}")
        } catch (ue: UnsupportedJwtException) {
            println("Unsupported JWT token -> Message: {}")
        } catch (ie: IllegalArgumentException) {
            println("JWT claims string is empty -> Message: {}")
        }

        return false
    }

    fun getUserNameFromJwtToken(jwt: String): String {
        return Jwts.parser()
                .setSigningKey(jwtSecretKey)
                .parseClaimsJws(jwt)
                .body.subject
    }
}