package com.example.demo.security.jwt

import com.example.demo.security.service.UserDetailServiceImp
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Created by Saeed on 8/1/2019.
 */
class JwtAuthTokenFilter(
        private val jwtProvider: JwtProvider,
        private val userService: UserDetailServiceImp
) : OncePerRequestFilter() {

    override fun doFilterInternal(
            request: HttpServletRequest,
            response: HttpServletResponse,
            filter: FilterChain) {

        val jwt = getJwt(request)
        if (jwt != "" && jwtProvider.validateJwtToken(jwt)) {

            val userDetails = userService.loadUserByUsername(
                    jwtProvider.getUserNameFromJwtToken(jwt))

            val auth = UsernamePasswordAuthenticationToken(
                    userDetails,
                    null,
                    userDetails.authorities)

            auth.details = WebAuthenticationDetailsSource().buildDetails(request)

            SecurityContextHolder.getContext().authentication = auth
        }


        filter.doFilter(request, response)
    }

    private fun getJwt(request: HttpServletRequest): String {

        val authHeader = request.getHeader("Authorization")

        if (authHeader != null) {
            return authHeader
        }
        return ""
    }
}