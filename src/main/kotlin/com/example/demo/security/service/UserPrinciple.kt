package com.example.demo.security.service

import com.example.demo.model.User
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

/**
 * Created by Saeed on 8/3/2019.
 */
class UserPrinciple : UserDetails {

    private var id: Int = 0
    private var username: String = ""
    private var password: String = ""
    private var authorities: MutableCollection<GrantedAuthority>? = null

    constructor()

    constructor(id: Int, username: String, password: String, authorities: MutableCollection<GrantedAuthority>) {
        this.id = id
        this.username = username
        this.password = password
        this.authorities = authorities
    }

    fun build(user: User, roles: MutableList<GrantedAuthority>): UserPrinciple {
        return UserPrinciple(user.id, user.username, user.password, roles)
    }


    fun getId(): Int = id

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return authorities!!
    }

    override fun isEnabled(): Boolean {
        return true
    }

    override fun getUsername(): String {
        return username
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun getPassword(): String {
        return password
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }
}