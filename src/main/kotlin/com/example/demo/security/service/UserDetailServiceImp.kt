package com.example.demo.security.service

import com.example.demo.model.User
import com.example.demo.repository.UserRepository
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service

/**
 * Created by Saeed on 7/29/2019.
 */

@Service
class UserDetailServiceImp(
        private val userRepository: UserRepository
) : UserDetailsService{

    override fun loadUserByUsername(username: String): UserDetails {

        // find user
        val user: User = userRepository.findByUsername(username)

        // generate granted authority list
        val roleList: MutableList<GrantedAuthority> = ArrayList()
        user.roles?.forEach { role ->
            val gAuth = SimpleGrantedAuthority(role.roleName.name)
            roleList.add(gAuth)
        }

//        return org.springframework.security.core.userdetails.User(
//                user.username,
//                user.password,
//                roleList
//        )
        return UserPrinciple().build(user, roleList)
    }
}